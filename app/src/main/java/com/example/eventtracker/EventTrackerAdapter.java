package com.example.eventtracker;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

//pripremanje adaptera za prikaz evenata u layout main activity-a
public class EventTrackerAdapter extends RecyclerView.Adapter<EventTrackerAdapter.MyViewHolder> {

    public List<EventTracker> eventTrackerList;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener, View.OnLongClickListener {
        TextView lastTime, lastLocation, number;
        Button eventButton;

        //pronalazi sve iteme u viewu po id-u
        MyViewHolder(View view) {
            super(view);
            lastTime = view.findViewById(R.id.lastHappened);
            lastLocation = view.findViewById(R.id.lastLocation);
            number = view.findViewById(R.id.eventNumOcu);
            eventButton = view.findViewById(R.id.clickEventButton);

            view.setOnTouchListener(this);
            view.setOnLongClickListener(this);

            //na klik se aktivira funkcija u main activity-u
            eventButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.eventClickButton(v, getAdapterPosition());
                }
            });
        }

        @Override
        public boolean onLongClick(View v) {
            return onClickListener.onLongClick(v, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return onClickListener.onTouch(v, event, getAdapterPosition());
        }
    }


    public EventTrackerAdapter(List<EventTracker> eventTrackerList) {
        this.eventTrackerList = eventTrackerList;
    }

    // trazi holder po id-u
    @Override
    @NotNull
    public EventTrackerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_view, parent, false);

        return new EventTrackerAdapter.MyViewHolder(itemView);
    }

    //sprema zadnje pojavljivanje dogadaja u holder
    @Override
    public void onBindViewHolder(EventTrackerAdapter.MyViewHolder holder, int position) {
        EventTracker eventTracker = eventTrackerList.get(position);
        if (!eventTracker.occurrences.isEmpty()) {
            Occurrence lastOccurrence = eventTracker.occurrences.get(eventTracker.occurrences.size() - 1);
            holder.lastTime.setText(lastOccurrence.timePoint.format(dateTimeFormatter));
            holder.lastLocation.setText(Math.round(lastOccurrence.lat * 1000.0) / 1000.0 + ", " + Math.round(lastOccurrence.lon * 1000.0) / 1000.0);
        } else {
            holder.lastTime.setText("Never happened");
            holder.lastLocation.setText("neverland");
        }

        holder.number.setText("" + eventTracker.occurrences.size() + "");
        holder.eventButton.setText(eventTracker.name);
    }

    @Override
    public int getItemCount() {
        return eventTrackerList.size();
    }

    public static EventAdapterListener onClickListener;

    public interface EventAdapterListener {
        void eventClickButton(View v, int position);

        boolean onItemClick(View view, int position);

        boolean onLongClick(View view, int position);

        boolean onTouch(View view, MotionEvent event, int position);
    }
}

