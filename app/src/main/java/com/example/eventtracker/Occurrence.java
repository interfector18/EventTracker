package com.example.eventtracker;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;

//baza za uzimanje pojavljivanja zadanog eventa
@Entity
class Occurrence {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public String eventTrackerName;

    @TypeConverters(LocalDateTimeConverter.class)
    public LocalDateTime timePoint;
    public double lat;
    public double lon;

    public Occurrence() {
    }

    public Occurrence(String eventTrackerName, LocalDateTime timePoint, double lat, double lon) {
        this.eventTrackerName = eventTrackerName;
        this.timePoint = timePoint;
        this.lat = lat;
        this.lon = lon;
    }

    public String getEventTrackerName() {
        return eventTrackerName;
    }

    public void setEventTrackerName(String eventTrackerName) {
        this.eventTrackerName = eventTrackerName;
    }

    public LocalDateTime getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(LocalDateTime timePoint) {
        this.timePoint = timePoint;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
