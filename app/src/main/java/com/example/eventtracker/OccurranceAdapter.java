package com.example.eventtracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
//pripremanje adaptera za prikaz evenata u layout detail activity-a
public class OccurranceAdapter extends RecyclerView.Adapter<OccurranceAdapter.MyViewHolder> {

    private List<Occurrence> occurranceList;
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView occurrenceNumber, dateTimeText, year, locationText;
        public Button delete;

        //pronalazi sve iteme u viewu po id-u
        public MyViewHolder(View view) {
            super(view);
            occurrenceNumber = view.findViewById(R.id.occurrenceNumber);
            dateTimeText = view.findViewById(R.id.dateTimeText);
            locationText = view.findViewById(R.id.locationText);
            delete = view.findViewById(R.id.deleteOccuranceButton);

            //poziva se delete funkcija u main activity-u
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.deleteButtonClick(v, getAdapterPosition());
                }
            });
        }

    }


    public OccurranceAdapter(List<Occurrence> OccurranceList) {
        this.occurranceList = OccurranceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ocurrance_view, parent, false);

        return new MyViewHolder(itemView);
    }

    //za svako pojavljivanje eventa ispisuje zadane detalje
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Occurrence occurrence = occurranceList.get(position);

        holder.occurrenceNumber.setText("# " + (position + 1));
        holder.dateTimeText.setText(occurrence.timePoint.format(dateTimeFormatter));
        holder.locationText.setText(Math.round(occurrence.lat * 1000.0) / 1000.0 + ", " + Math.round(occurrence.lon * 1000.0) / 1000.0);
    }

    @Override
    public int getItemCount() {
        return occurranceList.size();
    }

    public static MyAdapterListener onClickListener;

    public interface MyAdapterListener {
        void deleteButtonClick(View v, int position);
    }
}

