package com.example.eventtracker;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

//deklariranje objekta preko kojega pristupamo eventTracker bazi podataka
@Dao
public abstract class EventTrackerDao {

    @Query("SELECT * FROM eventtracker")
    abstract List<EventTracker> getAll();

    public void insertAll(List<EventTracker> eventTrackers) {
        for (EventTracker eventTracker : eventTrackers) {
            insertEventTracker(eventTracker);
            if (eventTracker.occurrences != null) {
                insertOccurrences(eventTracker.occurrences);
            }
        }
    }

    //uzimamo sva pojavljivanja nekog zadanog eventa
    @Query("SELECT * FROM Occurrence WHERE eventTrackerName = (:eventTrackerName)")
    abstract List<Occurrence> getOccurrences(String eventTrackerName);

    public List<EventTracker> getAllEvenTrackers() {
        List<EventTracker> eventTrackers = getAll();
        for (EventTracker eventTracker : eventTrackers) {
            eventTracker.occurrences = (ArrayList<Occurrence>) getOccurrences(eventTracker.name);
        }
        return eventTrackers;
    }

    @Insert
    abstract void insertEventTracker(EventTracker eventTracker);

    @Insert
    abstract void insertOccurrences(List<Occurrence> occurrences);

    @Insert
    abstract void _insertAll(EventTracker... eventTrackers);

    //brise sva pojavljivanja dogadaja o nekom zadanom eventu
    @Query("DELETE FROM Occurrence WHERE eventTrackerName = (:eventTrackerName)")
    abstract void deleteOccurrencesForEventTracker(String eventTrackerName);

    public void delete(EventTracker eventTracker)
    {
        deleteOccurrencesForEventTracker(eventTracker.name);
        _delete(eventTracker);
    }

    @Delete
    abstract void _delete(EventTracker eventTracker);
}
