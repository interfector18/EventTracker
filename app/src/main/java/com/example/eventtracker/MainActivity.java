package com.example.eventtracker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.ArraySet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

public class MainActivity extends AppCompatActivity {
    //dopustanja za GPS i NETWORK lokaciju
    private static final int MY_PERMISSION_GPS_REQUEST = 15;
    private static final int MY_PERMISSION_NETWORK_REQUEST = 16;

    //Deklariranje varijable Location koju pospremamo u LocationManager
    private LocationManager mLocationManager;
    private Location loc = new Location(LocationManager.GPS_PROVIDER);

    //Deklariranje event liste, eventNames te lastEventName za undo funkciju
    public List<EventTracker> events = new ArrayList<>();
    private Set<String> eventNames = new ArraySet<>();
    private List<String> lastEventName = new ArrayList<>();

    //Deklariranje baze
    public LocalDatabase localDatabase;

    //deklariranje varijable za vibraciju mobitela
    private Vibrator vibrator;

    //deklariranje varijable za navigaciju kroz aplikaciju
    private Button addButton;
    private Button undoButton;
    private RecyclerView recyclerView;
    private EventTrackerAdapter eventTrackerAdapter;
    private double lastX;
    private double lastY;
    public MediaPlayer mp;

    //Funkcije se pozivaju pri promjeni lokacije te se posprema zadnja lokacija
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            loc = location;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Popup tekst koji se pojavi pri vrhu ekrana nakon unosa novog eventa
        final Toast toast = Toast.makeText(getApplicationContext(), "New event added", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 200);

        //Spremamo dopustenja za lokaciju
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        //Spremamo zvuk prilikom klikanja na gumb
        mp = MediaPlayer.create(this, R.raw.click);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //provjeravamo imamo li doupustenje za uporabu GPS-a te uporabu Networka
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_GPS_REQUEST);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_NETWORK_REQUEST);
        } else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }

        //kreiramo lokalnu bazu podataka
        localDatabase = Room.databaseBuilder(getApplicationContext(), LocalDatabase.class, "local.db")
                .allowMainThreadQueries().build();

        //u lokalnu bazu spremamo sve informacije o eventima
        events = localDatabase.eventTrackerDao().getAllEvenTrackers();
        //u varijablu eventNames spremamo imena svih evenata
        for (EventTracker e : events) {
            eventNames.add(e.name);
        }

        //kreiramo recycle view koji koristimo za prikaz svih evenata
        recyclerView = findViewById(R.id.eventRecyclerView);


        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        //preko id-a pronalazimo addButton , undoButton te sakrivamo undoButton
        addButton = findViewById(R.id.addButton);
        undoButton = findViewById(R.id.undoButton);
        undoButton.setVisibility(View.INVISIBLE);

        //na ekranu prikazujemo buttone svih eventa pomocu varijable events
        eventTrackerAdapter = new EventTrackerAdapter(events);
        EventTrackerAdapter.onClickListener = new EventTrackerAdapter.EventAdapterListener() {

            //funkcija eventClickButton sluzi za spremanje potrebnih informacija o eventu koji se kliknuo
            @Override
            public void eventClickButton(View v, int position) {
                //palimo glazbu
                mp.start();
                //spremamo poziciju za evente
                EventTracker e = events.get(position);
                e.addOccurance(loc);
                //povecavamo broj pojavljivanja dogadaja
                e.occurrences.get(e.occurrences.size() - 1).id = (int) localDatabase.occurrenceDao().insertOccurance(e.occurrences.get(e.occurrences.size() - 1));
                //u lastEventName sprema ime zadnjeg eventa za undo funkciju
                lastEventName.add(e.getName());

                //refreshanje sucelja klikanjem na event
                eventTrackerAdapter.notifyItemChanged(position);
                RefreshUI();

                //pojavi se undoButton kada postoji barem 1 novi event
                if (lastEventName.size() > 0 && undoButton.getVisibility() != View.VISIBLE) {
                    undoButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public boolean onItemClick(View view, int position) {
                return false;
            }

            //prilikom dugog drzanja buttona otvori se meni
            @Override
            public boolean onLongClick(View view, final int position) {
                //mobitel zavibrira
                vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                //deklariramo intent za nove aktivnosti
                final Intent intentDetails = new Intent(getBaseContext(), DetailsActivity.class);
                final Intent intentLocation = new Intent(getBaseContext(), LocationActivity.class);
                //pronalazimo zadani layout preko id-a
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") View popupView = inflater.inflate(R.layout.popup_window, null);

                //kreiramo popup window sa zadanim parametrima
                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                boolean focusable = true;
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

                // prikazujemo popup window te pronalazimo buttone preko id-a
                popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, (int) lastX, (int) lastY + (int) view.getY());
                Button deleteButton = popupView.findViewById(R.id.deleteButton);
                Button detailsButton = popupView.findViewById(R.id.detailsButton);
                Button locationButton = popupView.findViewById(R.id.locationButton);

                //prilikom klika izvan popupa, nestaje popup
                popupView.setOnTouchListener(new View.OnTouchListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        popupWindow.dismiss();
                        return true;
                    }
                });

                //prilikom klika na delete button, brisemo event iz baze micemo ime i frefreshamo sucelje
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        EventTracker e = events.get(position);
                        localDatabase.eventTrackerDao().delete(e);
                        events.remove(e);
                        eventNames.remove(e.name);
                        popupWindow.dismiss();
                        RefreshUI();
                    }
                });

                //otvara novi activity koji pokazuje lokaciju svih evenata
                locationButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        EventTracker event = events.get(position);
                        intentLocation.putExtra("EVENT_NAME", event.name);
                        startActivity(intentLocation);
                    }
                });

                //otvara novi activity koji pokazuje detalje evenata
                detailsButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        EventTracker event = events.get(position);
                        intentDetails.putExtra("EVENT_NAME", event.name);
                        startActivity(intentDetails);
                    }
                });
                return true;
            }

            //postavlja popup desno od gdje smo pritisnuli gumb
            @Override
            public boolean onTouch(View view, MotionEvent event, int position) {
                lastX = event.getX();
                lastY = event.getY();
                return false;
            }
        };

        //
        recyclerView.setLayoutManager(new SpeedyLinearLayoutManager(getApplicationContext(), SpeedyLinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(eventTrackerAdapter);
        eventTrackerAdapter.eventTrackerList = events;
        eventTrackerAdapter.notifyDataSetChanged();

        //pritiskom pojavljuje se popup za unos teksta te se otvara tipkovnica
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // inflate the layout of the popup window
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") View popupView = inflater.inflate(R.layout.activity_addnewpopup, null);
                // create the popup window
                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                boolean focusable = true; // lets taps outside the popup also dismiss it
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

                popupWindow.showAtLocation(addButton, Gravity.BOTTOM, 0, (int) addButton.getY());
                Button addnewEventButton = popupView.findViewById(R.id.button);
                final EditText editTextNew = popupView.findViewById(R.id.editTextNewEvent);
                showKeyboard();
                editTextNew.requestFocus();
                // dismiss the popup window when touched
                popupView.setOnTouchListener(new View.OnTouchListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        popupWindow.dismiss();
                        return true;
                    }
                });

                //provjerava ako je zadano ime vec u bazi, te ako nije dodaje ga u bazu
                addnewEventButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String eventName = editTextNew.getText().toString();

                        if (!eventNames.contains(eventName)) {
                            toast.show();
                            addEvent(eventName);
                            popupWindow.dismiss();
                            hideKeyboard();
                        } else {
                            //ako ime vec postoji u bazi, javlja se popup sa greskom te zavibrira mobitel
                            vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("Name collision");
                            alertDialog.setMessage("Event already exist.\nPlease use a different name!");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    }
                });
            }
        });
        //klikom na undoButton brise se zadnje pojavljivanje eventa, refresha se sucelje i za zadnji event
        //postavlja event prije toga
        undoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EventTracker event, event2;
                String eventName = lastEventName.get(lastEventName.size() - 1);
                for (int i = 0; i < events.size(); i++) {
                    if (Objects.equals(events.get(i).name, eventName)) {
                        event = events.get(i);
                        Occurrence occurrence = event.occurrences.get(event.occurrences.size() - 1);
                        localDatabase.occurrenceDao().delete(occurrence);
                        event.RemoveLastOccurance();
                        eventTrackerAdapter.notifyItemChanged(i);
                        break;
                    }
                }
                lastEventName.remove(lastEventName.size() - 1);
                if (lastEventName.isEmpty()) {
                    undoButton.setVisibility(View.INVISIBLE);
                }
                RefreshUI();
            }
        });
    }

    //Ponovnim ulaskom u aplikaciju postavlja lastEventName na 0 te se undoButton sakriva
    @Override
    protected void onResume() {
        super.onResume();

        recyclerView = findViewById(R.id.eventRecyclerView);

        lastEventName.clear();
        undoButton.setVisibility(View.INVISIBLE);
        events = localDatabase.eventTrackerDao().getAllEvenTrackers();
        RefreshUI();
    }

    //provjerava je li imamo dopustenja za gps te network u locationManageru
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_GPS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                } else {
                    finish();
                    //android.os.Process.killProcess(android.os.Process.myPid());
                    System.out.println("Killao");
                    System.exit(0);
                }
            }
            case MY_PERMISSION_NETWORK_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                } else {
                    finish();
                    //android.os.Process.killProcess(android.os.Process.myPid());
                    System.out.println("Killao");
                    System.exit(0);
                }
            }
        }
    }

    //funkcija addEvent dodaje novi event koji posprema u bazu
    private EventTracker addEvent(String eventName) {
        EventTracker e = new EventTracker();
        e.name = eventName;
        events.add(e);
        localDatabase.eventTrackerDao().insertEventTracker(e);
        eventNames.add(eventName);

        return e;
    }

    //funkcija showKeyboard otvara tipkovnicu za unos naziva novog eventa
    public void showKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    //funkcija hideKeyboard skriva tipkovnicu nakon zavrsenog unosa
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    //funkcija RefreshUI poziva se prilikom refreshanja sucelja
    public void RefreshUI() {
       Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        eventTrackerAdapter.eventTrackerList = events;
        eventTrackerAdapter.notifyDataSetChanged();
    }
}
