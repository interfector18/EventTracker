package com.example.eventtracker;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

//deklariranje objekta preko kojega pristupamo Occurances bazi podataka
@Dao
public interface OccurrenceDao {

    //izaberemo sve occurance-s iz baze
    @Query("SELECT * FROM Occurrence")
    List<Occurrence> getAll();

    //izaberemo sve occurance iz baze za zadani eventTrackerName
    @Query("SELECT * FROM Occurrence WHERE eventTrackerName = :eventTrackerName")
    List<Occurrence> getAllForEventTrackerName(String eventTrackerName);

    @Insert
    long insertOccurance(Occurrence occurrence);

    @Insert
    void insertAll(Occurrence... occurrences);

    @Delete
    void delete(Occurrence occurrence);
}
