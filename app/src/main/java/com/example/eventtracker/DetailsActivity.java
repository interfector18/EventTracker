package com.example.eventtracker;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class DetailsActivity extends AppCompatActivity {

    //deklariranje potrebnih varijabli za prikaz detalja zadanog eventa
    private List<Occurrence> occurrences;
    private String eventName;
    private List<SliceValue> pieData = new ArrayList<>();
    private List<Integer> relatedEventsCounts = new ArrayList<>();
    private List<String> relatedEventsName = new ArrayList<>();
    public BarGraphSeries<DataPoint> seriesHourly;
    public BarGraphSeries<DataPoint> seriesDaily;
    public BarGraphSeries<DataPoint> seriesMonthly;

    private LocalDatabase localDatabase;

    private RecyclerView recyclerView;
    private OccurranceAdapter occurranceAdapter;


    private LinearLayout Layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //pronalazimo zadani layout preko layouta
        setContentView(R.layout.activity_details);
        //otvaramo lokalnu bazu
        localDatabase = Room.databaseBuilder(getApplicationContext(), LocalDatabase.class, "local.db")
                .allowMainThreadQueries().build();

        //deklariramo tekst koji se pojavljuje prilikom brisanja pojava dogadaja
        final Toast toast = Toast.makeText(getApplicationContext(), "Occurrence deleted!", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.CENTER, 0, 200);

        //uzimamo ime eventa koji smo prosljedili preko main activity-a
        eventName = getIntent().getStringExtra("EVENT_NAME");
        //u varijablu occurance sprememo sve informacije o zadanom eventu
        occurrences = localDatabase.occurrenceDao().getAllForEventTrackerName(eventName);

        recyclerView = findViewById(R.id.recyclerView);

        //Prikazujemo detalje o svakom pojavljivanju eventa te prilikom klika na delete button
        //brise se odredeno pojavljivanje dogadaja
        occurranceAdapter = new OccurranceAdapter(occurrences);
        OccurranceAdapter.onClickListener = new OccurranceAdapter.MyAdapterListener() {
            @Override
            public void deleteButtonClick(View v, int position) {
                toast.show();
                Occurrence occurrence = occurrences.get(position);
                occurrences.remove(occurrence);
                localDatabase.occurrenceDao().delete(occurrence);
                UpdateGraphs();
//                UpdateRelatedEvents();
                occurranceAdapter.notifyDataSetChanged();
            }
        };
        recyclerView.setLayoutManager(new SpeedyLinearLayoutManager(getApplicationContext(), SpeedyLinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(occurranceAdapter);
        occurranceAdapter.notifyDataSetChanged();

        UpdateGraphs();
        UpdateRelatedEvents();

    }

    //funkcija koja provjerava koji event je povezan s drugim eventima
    private void UpdateRelatedEvents() {
        List<EventTracker> allEvents = localDatabase.eventTrackerDao().getAllEvenTrackers();
        List<Occurrence> event = localDatabase.occurrenceDao().getAllForEventTrackerName(eventName);

        int counts;
        for (int j = 0; j < allEvents.size(); j++) {
            counts = 0;
            for (int i = 0; i < event.size(); i++) {
                for (int k = 0; k < allEvents.get(j).occurrences.size(); k++) {
                    if (allEvents.get(j).occurrences.get(k).timePoint.isAfter(event.get(i).timePoint.minusMinutes(1)) &&
                            allEvents.get(j).occurrences.get(k).timePoint.isBefore(event.get(i).timePoint.plusMinutes(1)) &&
                            !Objects.equals(eventName, allEvents.get(j).getName())) {
                        if (allEvents.get(j).occurrences.get(k).lon < event.get(i).lon + 0.05 &&
                                allEvents.get(j).occurrences.get(k).lon > event.get(i).lon - 0.05 &&
                                allEvents.get(j).occurrences.get(k).lat < event.get(i).lat + 0.05 &&
                                allEvents.get(j).occurrences.get(k).lat > event.get(i).lat - 0.05) {
                            if (relatedEventsName.contains(allEvents.get(j).getName())) {
                                counts++;
                            } else {
                                relatedEventsName.add(allEvents.get(j).getName());
                                counts++;
                            }

                        }
                    }
                }
            }
            if (counts > 0) {
                relatedEventsCounts.add(counts);
            }
        }

        //izradujemo pie chart te prikazujemo podatke o povezanim dogadajima
        Layout = findViewById(R.id.layout);

        for (int i = 0; i < relatedEventsName.size(); i++) {
            pieData.add(addValue(i));
        }


        PieChartView pieChartView = findViewById(R.id.chart);
        final PieChartData pieChart = new PieChartData(pieData);
        pieChart.setHasLabels(true);
        pieChartView.setPieChartData(pieChart);
        if (pieData.size() == 0) {
            Layout.removeView(findViewById(R.id.chartText));
            Layout.removeView(pieChartView);
        }
    }

    //funkcija za ispis grafa po satu, danu i mjesecu
    public void UpdateGraphs() {
        GraphView graphHourly = findViewById(R.id.graphHourly);
        GraphView graphDaily = findViewById(R.id.graphDaily);
        GraphView graphMonthly = findViewById(R.id.graphMonthly);

        seriesHourly = new BarGraphSeries<>(generateDataHourly());
        seriesDaily = new BarGraphSeries<>(generateDataDaily());
        seriesMonthly = new BarGraphSeries<>(generateDataMonthly());

        graphHourly.removeAllSeries();
        graphDaily.removeAllSeries();
        graphMonthly.removeAllSeries();

        graphHourly.addSeries(seriesHourly);
        graphHourly.getGridLabelRenderer().setHorizontalAxisTitle("Hours");
        graphHourly.getViewport().setXAxisBoundsManual(true);
        graphHourly.getViewport().setMaxX(23);
        graphHourly.getViewport().setScalable(true);

        graphDaily.addSeries(seriesDaily);
        graphDaily.getGridLabelRenderer().setHorizontalAxisTitle("Days");
        graphDaily.getViewport().setXAxisBoundsManual(true);
        graphDaily.getViewport().setMaxX(31);
        graphDaily.getViewport().setMinX(1);
        graphDaily.getViewport().setScalable(true);

        graphMonthly.addSeries(seriesMonthly);
        graphMonthly.getViewport().calcCompleteRange();
        graphMonthly.getGridLabelRenderer().setHorizontalAxisTitle("Months");
        graphMonthly.getViewport().setXAxisBoundsManual(true);
        graphMonthly.getViewport().setScalable(true);
        graphMonthly.getViewport().setMaxX(12);
        graphMonthly.getViewport().setMinX(1);
    }

    //funkcija koja generira podatke po satu
    private DataPoint[] generateDataHourly() {
        int[] hours = new int[24];
        DataPoint[] values = new DataPoint[24];

        for (int i = 0; i < occurrences.size(); i++) {
            hours[occurrences.get(i).timePoint.getHour()]++;
        }

        for (int j = 0; j < 24; j++) {
            values[j] = new DataPoint(j, hours[j]);
        }
        return values;
    }

    //funkcija koja generira podatke po danu
    private DataPoint[] generateDataDaily() {
        int[] days = new int[31];
        DataPoint[] values = new DataPoint[31];

        for (int i = 0; i < occurrences.size(); i++) {
            days[occurrences.get(i).timePoint.getDayOfMonth() - 1]++;
        }

        for (int j = 0; j < 31; j++) {
            values[j] = new DataPoint(j + 1, days[j]);
        }
        return values;
    }

    //funkcija koja generira podatke po mjesecu
    private DataPoint[] generateDataMonthly() {
        int[] months = new int[12];
        DataPoint[] values = new DataPoint[12];

        for (int i = 0; i < occurrences.size(); i++) {
            months[occurrences.get(i).timePoint.getMonthValue() - 1]++;
        }

        for (int j = 0; j < 12; j++) {
            values[j] = new DataPoint(j + 1, months[j]);
        }
        return values;
    }

    //funkcija za generiranje vrijednosti i nasumicno odabranu boje pieCharta
    private SliceValue addValue(int i) {
        Random rand = new Random();
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        return new SliceValue(relatedEventsCounts.get(i), Color.rgb(r, g, b)).setLabel(relatedEventsName.get(i) + ": " + relatedEventsCounts.get(i));
    }


}
