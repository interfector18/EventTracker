package com.example.eventtracker;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {EventTracker.class, Occurrence.class}, version = 1)
public abstract class LocalDatabase extends RoomDatabase {
    public abstract EventTrackerDao eventTrackerDao();

    public abstract OccurrenceDao occurrenceDao();
}
