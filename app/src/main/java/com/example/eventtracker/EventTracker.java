package com.example.eventtracker;

import android.location.Location;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

//baza podataka koja sprema sve evente i podatke o eventima
@Entity
public class EventTracker {

    @PrimaryKey
    @NotNull
    public String name;

    @Ignore
    public ArrayList<Occurrence> occurrences = new ArrayList<>();

    public EventTracker() {
    }

    public EventTracker(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Occurrence> getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(ArrayList<Occurrence> occurrences) {
        this.occurrences = occurrences;
    }

    //dodaje novi dogadaj u bazu i posprema podatke o njemu
    public boolean addOccurance(Location loc) {
        Occurrence o = new Occurrence();

        o.eventTrackerName = name;
        o.timePoint = LocalDateTime.now();
        o.lat = loc.getLatitude();
        o.lon = loc.getLongitude();

        occurrences.add(o);

        return true;
    }

    public boolean addOccurance(Occurrence occurrence) {
        occurrences.add(occurrence);
        return true;
    }

    public void RemoveLastOccurance() {
        occurrences.remove(occurrences.size()-1);
    }
}
