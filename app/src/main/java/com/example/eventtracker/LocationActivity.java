package com.example.eventtracker;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

//activity za prikaz lokacije odabranog eventa
public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocalDatabase localDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        localDatabase = Room.databaseBuilder(getApplicationContext(), LocalDatabase.class, "local.db")
                .allowMainThreadQueries().build();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    //ako je google map dostupna poziva se funkcija
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

        //uzima ime eventa kojeg smo prosljedili preko main activity-a
        String eventName = getIntent().getStringExtra("EVENT_NAME");

        //uzima sve pojavljivanja zadanog eventa iz baze
        List<Occurrence> occurrences = localDatabase.occurrenceDao().getAllForEventTrackerName(eventName);

        double avgX = 0, avgY = 0;

        //postavljamo marker za svaki event te dodajemo ime i vrijeme u opis markera
        for (Occurrence occurrence : occurrences) {
            double x = occurrence.lat, y = occurrence.lon;
            avgX += x;
            avgY += y;
            LatLng v = new LatLng(x, y);
            String title = occurrence.eventTrackerName + ", at: " + occurrence.timePoint.format(dateTimeFormatter);
            mMap.addMarker(new MarkerOptions().position(v).title(title));
        }
        avgX /= occurrences.size();
        avgY /= occurrences.size();

        //zumira na podrucje oko markera
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(avgX, avgY), 4));
    }
}
